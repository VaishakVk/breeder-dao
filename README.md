# Breeder DAO Axie GraphQL API

## Steps to Install

-   Clone the repository
-   Navigate to folder on the local system
-   Run `yarn`. This will install all the dependencies.
-   Create `.env` file in the root directory and enter the secrets by referring to `.env.example`.
-   Run `yarn start` to start the application.
-   Postman documentation is available in `postman` folder

## Limitations

-   No authentication is required.
-   Pagination is not allowed.
-   Only read functions of the smart contract can be interacted since Write functions would require a real wallet.
