import { Base } from "../../models/Base";
import { Resolver, Mutation, Query, Arg } from "type-graphql";
import { AxieService } from "./Axie.service";
import { AxieFetchInput } from "./dto/getAxies.dto";
import { SmartContractFetchInput } from "./dto/getSmartContractFunction.dto";
import { SmartContractFetchInputResponse } from "./dto/getSmartContractFunctionResult.dto";

@Resolver()
export class AxieResolver {
    @Query(() => [Base])
    async axies(
        @Arg("fetchQuery") fetchQuery: AxieFetchInput
    ): Promise<Base[]> {
        return AxieService.fetchAxiesFromDb(fetchQuery);
    }

    @Mutation(() => String)
    async syncAxieDetails(): Promise<string> {
        await AxieService.fetchAxiesAndInsertToDb();
        return "Ok";
    }

    @Mutation(() => SmartContractFetchInputResponse)
    async smartContractRead(
        @Arg("smartContractReadQuery")
        smartContractReadQuery: SmartContractFetchInput
    ): Promise<SmartContractFetchInputResponse> {
        return AxieService.querySmartContract(smartContractReadQuery);
    }
}
