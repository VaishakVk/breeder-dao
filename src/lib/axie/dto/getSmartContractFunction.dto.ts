import { IsOptional } from "class-validator";
import { InputType, Field } from "type-graphql";

@InputType()
export class SmartContractFetchInput {
    @Field()
    functionName: string;

    @Field(() => [String], { nullable: true })
    @IsOptional()
    params?: string[];
}
