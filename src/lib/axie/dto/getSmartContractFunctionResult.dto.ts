import { ObjectType, Field } from "type-graphql";
import JSON from "graphql-type-json";

@ObjectType()
export class SmartContractFetchInputResponse {
    @Field(() => JSON)
    response: JSON;
}
