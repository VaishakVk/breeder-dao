import { InputType, Field } from "type-graphql";
import { Base } from "src/models/Base";

@InputType()
export class AxieFetchInput implements Partial<Base> {
    @Field({ nullable: true })
    stage: number;

    @Field({ nullable: true })
    class: string;
}
