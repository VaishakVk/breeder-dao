import { AxieService as Axie } from "../thirdParty/axieGQL/axie.service";

import { DBService } from "../../db/db.service";
import { AxieMultiInsertPayload, IAxieResponse } from "src/common/types";
import { AxieClasses } from "src/common/enum";
import { Base } from "src/models/Base";
import { AxieFetchInput } from "./dto/getAxies.dto";
import { SmartContractFetchInput } from "./dto/getSmartContractFunction.dto";
import { Web3Service } from "../web3/web3.service";
import { SmartContractFetchInputResponse } from "./dto/getSmartContractFunctionResult.dto";

export class AxieService {
    /**
     * @description Groups the Axie records according to classes and creates payload to insert
     * @param data IAxieResponse[]
     * @returns
     */
    static formatToDBInsertPayload(
        data: IAxieResponse[]
    ): AxieMultiInsertPayload {
        const payload: AxieMultiInsertPayload = {
            Aquatic: [],
            Beast: [],
            Plant: [],
            Bird: [],
            Bug: [],
            Reptile: [],
            Mech: [],
            Dawn: [],
            Dusk: [],
        };

        for (let i = 0; i < data.length; i++) {
            const { id, name, stage, class: className, auction } = data[i];
            payload[className as AxieClasses].push({
                axieId: id,
                stage,
                class: className,
                name,
                price: auction.currentPriceUSD,
            });
        }

        return payload;
    }

    /**
     * @description Fetches Records from Axie Graphql service and inserts to DB
     */
    static async fetchAxiesAndInsertToDb() {
        await DBService.cleanDB();
        const axies = await Axie.getAxies(300);
        const payload = this.formatToDBInsertPayload(axies);
        await DBService.insertToDB(payload);
    }

    /**
     * @description Fetches Records from DB
     * @param fetchQuery AxieFetchInput
     * @returns
     */
    static async fetchAxiesFromDb(fetchQuery: AxieFetchInput): Promise<Base[]> {
        return DBService.fetchAxies(fetchQuery);
    }

    /**
     * @description Queries function from smart contract
     * @param fetchQuery SmartContractFetchInput
     * @returns
     */
    static async querySmartContract(
        fetchQuery: SmartContractFetchInput
    ): Promise<SmartContractFetchInputResponse> {
        return Web3Service.querySmartContract(fetchQuery);
    }
}
