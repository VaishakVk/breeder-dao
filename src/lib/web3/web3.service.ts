import { readFileSync } from "fs";
import path from "path";
import Web3 from "web3";
import { SmartContractFetchInput } from "../axie/dto/getSmartContractFunction.dto";
import { SmartContractFetchInputResponse } from "../axie/dto/getSmartContractFunctionResult.dto";

export class Web3Service {
    static async initContract() {
        const web3 = new Web3(process.env.WEB3_URL as string);
        const contractJson = readFileSync(
            path.join(__dirname, "../thirdParty/contract/abi.json")
        ).toString();
        const abi = JSON.parse(contractJson);
        const contractAddress = process.env.ADDRESS;
        const AxieContract = new web3.eth.Contract(abi, contractAddress);
        return { AxieContract, contractJson, web3 };
    }

    static async querySmartContract(
        inputQuery: SmartContractFetchInput
    ): Promise<SmartContractFetchInputResponse> {
        if (!inputQuery.params) inputQuery.params = [];
        const { AxieContract } = await this.initContract();
        const smartContractResult = await AxieContract.methods[
            inputQuery.functionName
        ](...inputQuery.params).call();

        const response = JSON.parse(
            JSON.stringify({ [inputQuery.functionName]: smartContractResult })
        );
        return { response };
    }
}
