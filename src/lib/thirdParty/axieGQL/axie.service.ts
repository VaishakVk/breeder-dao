import axios from "axios";
import { IAxieResponse } from "src/common/types";

export class AxieService {
    private static apiURL = "https://graphql-gateway.axieinfinity.com/graphql";

    static async getAxies(records: number): Promise<IAxieResponse[]> {
        const numberOfRequests = records / 100;
        const requests = [];
        for (let i = 0; i < numberOfRequests; i++) {
            requests.push(
                axios.post<{ data: { axies: { results: IAxieResponse[] } } }>(
                    this.apiURL,
                    {
                        query: `query GetAxieBriefList($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {  
                            axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {
                                results {
                                    id  
                                    name  
                                    stage  
                                    class  
                                    auction {
                                        currentPriceUSD
                                    }
                                }
                            }
                        }`,
                        variables: {
                            from: i * 100,
                            size: 100,
                            auctionType: "Sale",
                            sort: "PriceAsc",
                            criteria: {
                                classes: [
                                    "Beast",
                                    "Aquatic",
                                    "Plant",
                                    "Bird",
                                    "Bug",
                                    "Reptile",
                                    "Mech",
                                    "Dawn",
                                    "Dusk",
                                ],
                            },
                        },
                    },
                    {
                        headers: {
                            "Content-Type": "application/json",
                        },
                    }
                )
            );
        }

        const data = await Promise.all(requests);
        const resultsArray = data.map((res) => res.data?.data?.axies?.results);

        return [].concat.apply([], resultsArray);
    }
}
