import { AxieClasses } from "./enum";

export type AxieMultiInsertPayload = {
    [key in AxieClasses]: RequestType[];
};

export type RequestType = {
    axieId: string;
    name: string;
    stage: number;
    class: string;
    price: number;
};

export type IAxieResponse = {
    id: string;
    name: string;
    stage: number;
    class: string;
    auction: {
        currentPriceUSD: number;
    };
};
