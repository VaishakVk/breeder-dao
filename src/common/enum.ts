export enum AxieClasses {
    aquatic = "Aquatic",
    beast = "Beast",
    plant = "Plant",
    bird = "Bird",
    bug = "Bug",
    reptile = "Reptile",
    mech = "Mech",
    dawn = "Dawn",
    dusk = "Dusk",
}
