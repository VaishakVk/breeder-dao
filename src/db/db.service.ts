import { AxieMultiInsertPayload } from "src/common/types";
import { Base } from "src/models/Base";
import { AquaticClassModel } from "../models/AquaticClass";
import { BeastClassModel } from "../models/BeastClass";
import { BirdClassModel } from "../models/BirdClass";
import { BugClassModel } from "../models/BugClass";
import { DawnClassModel } from "../models/DawnClass";
import { DuskClassModel } from "../models/DuskClass";
import { MechClassModel } from "../models/MechClass";
import { PlantClassModel } from "../models/PlantClass";
import { ReptileClassModel } from "../models/ReptileClass";

export class DBService {
    static async cleanDB(): Promise<void> {
        await Promise.all([
            AquaticClassModel.deleteMany(),
            BeastClassModel.deleteMany(),
            PlantClassModel.deleteMany(),
            BirdClassModel.deleteMany(),
            BugClassModel.deleteMany(),
            ReptileClassModel.deleteMany(),
            MechClassModel.deleteMany(),
            DawnClassModel.deleteMany(),
            DuskClassModel.deleteMany(),
        ]);
    }

    static async insertToDB(payload: AxieMultiInsertPayload): Promise<void> {
        await Promise.all([
            AquaticClassModel.insertMany(payload.Aquatic),
            BeastClassModel.insertMany(payload.Beast),
            PlantClassModel.insertMany(payload.Plant),
            BirdClassModel.insertMany(payload.Bird),
            BugClassModel.insertMany(payload.Bug),
            ReptileClassModel.insertMany(payload.Reptile),
            MechClassModel.insertMany(payload.Mech),
            DawnClassModel.insertMany(payload.Dawn),
            DuskClassModel.insertMany(payload.Dusk),
        ]);
    }

    static async fetchAxies(query: Record<any, any>): Promise<Base[]> {
        const res = await Promise.all([
            AquaticClassModel.find<Base>(query),
            BeastClassModel.find<Base>(query),
            PlantClassModel.find<Base>(query),
            BirdClassModel.find<Base>(query),
            BugClassModel.find<Base>(query),
            ReptileClassModel.find<Base>(query),
            MechClassModel.find<Base>(query),
            DawnClassModel.find<Base>(query),
            DuskClassModel.find<Base>(query),
        ]);

        const mergedArray = [].concat.apply([], res) as Base[];
        return mergedArray.sort((a, b) => a.price - b.price);
    }
}
