import { ApolloServer } from "apollo-server-express";
import { ApolloServerPluginLandingPageGraphQLPlayground } from "apollo-server-core";
import Express from "express";
import "reflect-metadata";
import { buildSchema } from "type-graphql";
import { connect } from "mongoose";
import { AxieResolver } from "./lib/axie/axie.resolver";

import * as dotenv from "dotenv";
dotenv.config();

const main = async () => {
    const schema = await buildSchema({
        resolvers: [AxieResolver],
        emitSchemaFile: true,
        validate: false,
    });
    const connectionUrl = process.env.DB_URL as string;

    const mongoose = await connect(connectionUrl);
    await mongoose.connection;

    const server = new ApolloServer({
        schema,
        plugins: [ApolloServerPluginLandingPageGraphQLPlayground],
    });

    const app = Express();

    await server.start();

    server.applyMiddleware({ app });

    app.listen({ port: process.env.PORT }, () =>
        console.log(
            `🚀 Server ready and listening at ==> http://localhost:${process.env.PORT}${server.graphqlPath}`
        )
    );
};

main().catch((error) => {
    console.log(error, "error");
});
