import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class PlantClass extends BaseModel {}
export const PlantClassModel = getModelForClass(PlantClass);
