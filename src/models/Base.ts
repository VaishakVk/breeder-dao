import { ObjectType, Field, ID } from "type-graphql";
import { prop as Property } from "@typegoose/typegoose";

@ObjectType({ description: "The Base model" })
export class Base {
    @Field(() => ID)
    id: string;

    @Field()
    @Property()
    name: string;

    @Field()
    @Property()
    axieId: string;

    @Field()
    @Property()
    stage: number;

    @Field()
    @Property()
    class: string;

    @Field()
    @Property()
    price: number;
}

export const BaseModel = Base;
