import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class DawnClass extends BaseModel {}
export const DawnClassModel = getModelForClass(DawnClass);
