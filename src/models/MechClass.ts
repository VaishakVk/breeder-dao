import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class MechClass extends BaseModel {}
export const MechClassModel = getModelForClass(MechClass);
