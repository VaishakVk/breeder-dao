import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class BeastClass extends BaseModel {}
export const BeastClassModel = getModelForClass(BeastClass);
