import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class BugClass extends BaseModel {}
export const BugClassModel = getModelForClass(BugClass);
