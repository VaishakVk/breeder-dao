import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class BirdClass extends BaseModel {}
export const BirdClassModel = getModelForClass(BirdClass);
