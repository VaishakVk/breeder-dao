import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class DuskClass extends BaseModel {}
export const DuskClassModel = getModelForClass(DuskClass);
