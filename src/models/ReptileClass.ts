import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class ReptileClass extends BaseModel {}
export const ReptileClassModel = getModelForClass(ReptileClass);
