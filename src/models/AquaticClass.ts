import { getModelForClass } from "@typegoose/typegoose";
import { BaseModel } from "./Base";

class AquaticClass extends BaseModel {}
export const AquaticClassModel = getModelForClass(AquaticClass);
